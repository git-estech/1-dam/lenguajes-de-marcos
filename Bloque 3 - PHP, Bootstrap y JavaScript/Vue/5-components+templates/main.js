const Hola = {
	template:
	`
		<h3>Hola desde el componente</h3>
	`,
}

const Num = {
	props: {
		numero: {
			type: Number,
			required: true
		}
	},
	template:
	`
		<div>{{ numero }}</div>
	`
}

const app = Vue.createApp({
	components: { // componentes vue a nivel local
		Hola,
		Num,
		// FooterBanco,
		// Titulo
	},
	template:
	`
		<Hola />
		<input v-model="value"> {{ value }}
		
		<div v-for="num in numeros">
			<num :numero="num" />
		</div>
		<footer-banco />
	`,
	data() {
		return {
			value: "Usuario",
			numeros: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
		}
	},
	computed: {
	},
	watch: {
	},
	methods: {
	}
})

app.component('FooterBanco', {
	template:
	`
		<div>
			<h3>Pie de página, año 2022</h3>
		</div>
	`
})

app.component('Titulo', {
	template:
	`
		<h3>{{ subtitulo }}</h3>
	`,
	data() {
		return {
			subtitulo: 'Listado de Frameworks JavaScript'
		}
	}
})

app.mount('#app');
