Vue.createApp({
	data() {
		return {
			current_task: "",
			tasks: []
		}
	},
	methods: {
		addTask() {
			if (this.current_task === null || this.current_task === "") {
				window.alert("no puede estar vacío")
				return
			}
			this.tasks.push(this.current_task)
			this.current_task = ""
		}
	}
}).mount('#app');