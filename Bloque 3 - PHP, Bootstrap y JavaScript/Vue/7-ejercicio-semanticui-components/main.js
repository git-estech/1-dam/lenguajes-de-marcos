Vue.createApp({})
	.component('notification-message', {
		template:
		`
		<div class="ui message" :class="tipo" v-if="!hidden">
			<i class="close icon" @click="hide"></i>
			<div class="header">{{ header }}</div>
			{{ message }}
		</div>
		`,
		props: {
			tipo:    { type: String, default: 'info' },
			header:  { type: String, default: 'Información' },
			message: {  },
		},
		data() {
			return { hidden: false }
		},
		methods: {
			hide() { this.hidden = true }
		}
	}).mount('#app')