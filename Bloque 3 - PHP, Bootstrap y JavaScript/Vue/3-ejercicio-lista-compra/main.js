Vue.createApp({
	data() {
		return {
			utiles:[
				{
					util: '10 Cuadernos',
					comprado: true
				},
				{
					util: '15 lápices',
					comprado: true
				},
				{
					util: '5 cajas de colores',
					comprado: false
				}
			]
		}
	},
	computed: {
	},
	watch: {
	},
	methods: {
		toggle(util) {
			util.comprado = !util.comprado
		}
	}
}).mount('#lista-compras');