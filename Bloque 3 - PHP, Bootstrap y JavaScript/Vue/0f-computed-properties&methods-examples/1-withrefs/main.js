let apellido = 'snow'

Vue.createApp({
	data() {
		return {
			nombre: 'John'
		}
	},
	computed: {
		fullNameComputed() {
			return this.nombre + ' ' + apellido
		}
	},
	methods: {
		guardarApellido() {
			apellido = this.$refs.elApellido.value // similar a querySelector("#apellido").value
		}
	}
}).mount('#app');