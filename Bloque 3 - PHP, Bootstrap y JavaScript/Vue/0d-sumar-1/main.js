Vue.createApp({
	data() {
		return {
			numero: 0,
			numero_suma: 1
		}
	},
	methods: {
		addOne() {
			this.numero += 1;
		},
		setValue(number) {
			this.numero = +number;
		}
	}
}).mount('#app');