const framework = {
  template: '<li><slot></slot></li>'
}

const app = Vue.createApp({
  components: {
    framework
  },
  //template: '<div><ul><framework>hola!</framework></ul></div>',
  template: '<div><ul><framework v-for="fra in frameworks" :key="fra.id">{{fra.titulo}}</framework></ul></div>',
  data(){
    return {
      frameworks: [
        { id: 1, titulo: 'Vue' },
        { id: 2, titulo: 'Angular' },
        { id: 3, titulo: 'React' }
      ]
    }
  }
}).mount('#app');
