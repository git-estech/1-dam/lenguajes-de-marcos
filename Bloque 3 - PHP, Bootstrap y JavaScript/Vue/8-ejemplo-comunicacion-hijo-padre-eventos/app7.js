const Num = {
    props: {
      numero:{
        type: Number,
        required:true
      } 
    },
    emits:['BotonNumero'],
    template: `
      <button :class="getClass(numero)" :title="num" @click="hacerClick">
        {{ numero }}
      </button>
    `,
    methods: {
      hacerClick(){
          this.$emit('BotonNumero', this.numero);
      },
        getClass(num){
          if(this.esPar(num)){
            return 'rojo';
          }
          return 'azul';
        },
        esPar(num){
          return num % 2 === 0
        }  
    }
  }
  
  const app = Vue.createApp({
    components: {
      Num
    },
    data(){
      return {
        numeros:[1,2,3,4,5,6,7,8,9,10],
        numerosRecibidos: []   
        }
      },
    template: `
        <num v-for="num in numeros" :numero="num" @BotonNumero="recibir" :key="num"/> 
        <h3>Números recibidos:</h3>
        <num v-for="num in numerosRecibidos" :numero="num"/> 
      `,
      methods:{
        recibir(num){
         this.numerosRecibidos.push(num)
        }
      }
  }).mount('#app');