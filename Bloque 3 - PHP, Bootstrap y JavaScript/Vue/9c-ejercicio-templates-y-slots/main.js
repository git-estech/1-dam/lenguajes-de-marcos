Vue.createApp({})
	.component('notification-message', {
		template: '#notification-message-template',
		props: {
			tipo:    { type: String, default: 'info' },
			header:  { type: String, default: 'Información' },
			message: {  },
		},
		data() {
			return { hidden: false }
		},
		methods: {
			hide() { this.hidden = true }
		}
	}).mount('#app')