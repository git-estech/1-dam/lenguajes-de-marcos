const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
	transpileDependencies: true,

	// En realidad debería ser "./" pero al compilar se elimina el ./ así que
	// mejor se pone doble para que solo se elimine el primero.

	// Gracias a: https://github.com/vuejs/vue-cli/issues/1623#issuecomment-729086789
	publicPath: process.env.NODE_ENV === 'production' ? '././' : './',
})
