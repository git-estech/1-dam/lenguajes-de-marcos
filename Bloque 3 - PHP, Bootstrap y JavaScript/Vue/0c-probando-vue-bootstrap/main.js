Vue.createApp({
	data() {
		return {
			cuenta: 'Vista',
			titular: 'Oswaldo',
			saldo: 500,

			enlaceBanca: 'https://mibanco.com/dashboard',
			enlaceHTMLBanca: '<a href="https://mibanco.com/dashboard">Acceso banco 3</a>',

			estado: true,

			font_size: 20,

			parentMessage: 'Parent',
			// items: [
			// 	{ message: 'foo' },
			// 	{ message: 'bar' }
			// ],
			//
			// myObject: {
			// 	title:  'How to do lists in Vue',
			// 	author: 'Jane Doe',
			// 	publishedAt: '2016-04-10'
			// },
			servicios: [
				'transferencias',
				'pagos',
				'giros'
			],
			habilitados: [
				{ nombre: 'Carlos' },
				{ nombre: 'María'  }
			]
		}
	},
	methods: {
		agregarSaldo() {
			this.saldo += 100;
		},
		disminuirSaldo() {
			if (this.saldo <= 0) {
				alert("Llegaste al final");
				return
			}
			this.saldo -= 100;
		}
	}
}).mount('#app');