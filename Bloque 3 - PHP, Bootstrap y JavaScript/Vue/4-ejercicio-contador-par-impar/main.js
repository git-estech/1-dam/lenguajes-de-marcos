Vue.createApp({
	data() {
		return {
			num: 0,
			arr_nums: [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
		}
	},
	computed: {
		isEvenOrOddString() {
			return (this.num % 2 === 0) ? 'Par' : 'Impar'
		}
	},
	watch: {
	},
	methods: {
		increaseNum() {
			this.num  += 1
		},
		isEvenOrOddCss(number) {
			return (number % 2 === 0) ? 'color: blue' : 'color: red'
		}
	}
}).mount('#app');