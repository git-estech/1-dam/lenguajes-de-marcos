Vue.createApp({
	data() {
		return {
			saldo: 500
		}
	},
	computed: {
		priceComputedCss() {
			return this.saldo > 100 ? 'text-success' : 'text-danger'
		}
	},
	methods: {
		agregarSaldo() {
			this.saldo += 100;
		},
		disminuirSaldo() {
			if (this.saldo <= 0) {
				alert("Llegaste al final");
				return
			}
			this.saldo -= 100;
		}
	}
}).mount('#app');