Vue.createApp({
	data() {
		return {
			name: "",
		}
	},
	computed: {
		name_length: function() {
			return this.name.length
		}
	},
	watch: {
	},
	methods: {
	}
}).mount('#app');