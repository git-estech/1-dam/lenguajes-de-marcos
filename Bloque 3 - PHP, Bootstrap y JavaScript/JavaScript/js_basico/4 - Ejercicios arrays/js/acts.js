/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/


'use strict';



/** 1.
 *  Crea un array llamado meses que almacene el nombre de los doce meses del año.
 *  Mostrar por pantalla los doce nombres utilizando la función console.log() con
 *  el siguiente formato:
 * 
 *    "Enero está en el índice 0 dentro de Enero,Febrero,Marzo,Abril,Mayo,Junio,
 *    Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre"
 */

function act1()
{
    let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
    'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    let num = 0;

    console.log(`${meses[num]} está en el índice ${num} dentro de ${meses.join(',')} `);

    // tenia que ser con meses.foreach
}



/** 2.
 *  Crea una función que reciba un parámetro (un dni), y devuelva la letra del
 *  mismo. Si el dni pasado tiene algún error devolverá "".
 */

function act2()
{
    // es la act 2 igual que la cuatro? no lo entiendo ni con el enunciado
    // actualizado
}



/** 3.
 *  Realiza un script que pida por teclado 3 edades y 3 nombres e indique el
 *  nombre del mayor.
 */

function act3()
{
    let nombre, edad;
    let numPersonas = 3;

    for (let i = 0; i != numPersonas; i++) {
        nombre[i] = window.prompt('Escribe el nombre');
        edad[i]   = window.prompt(`Escriba la edad de ${nombre[i]}`);
    }

    // todo: ya sabes, añadir cosica
        //return name[age.inde]

}



/** 4.
 *  Realizar una página con un script que calcule el valor de la letra de un
 *  número de DNI.
 * 
 *  El algoritmo para calcular la letra del dni es el siguiente:
 * 
 *  Algoritmo para calcular la letra del dni es el siguiente:
 *    El número debe ser entre 0 y 99999999
 *    Debemos calcular el resto de la división entera entre el número y el número 23.
 *    Según el resultado, de 0 a 22, le corresponderá una letra de las siguientes:
 *    (T, R, W, A, G, M, Y, F, P, D, X, B, N, J, Z, S, Q, V, H, L, C, K, E)
 * 
 *  Si lo introducido no es un número deberá indicarse con un alert y volver a preguntar.
 *  Deberá de repetirse el proceso hasta que el usuario pulse “cancelar”.
 */

function act4()
{
    // TODO: cambiar a un [bucle do while](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/do...while)


    let dniValue = parseInt(window.prompt('Escribe tu DNI'));
    let dniLetter = "";

    // 1. El número debe ser entre 0 y 99999999
    if (dniValue != NaN && dniValue > 0 && dniValue < 99999999) {

        /**
         * 1. A giveNum() se le pasa el dni _i_ y devuelve el resto de i % 23
         * 2. A giveLetter() se le pasa ese resto y devuelve la letra en esa pos. del array
         * 3. A giveAlert() se le pasa la letra e _i_, para hacer un alert en la pag.
         */
        giveAlert(dniValue, giveLetter(giveNum(dniValue)));

    } else {
        let confirmBool = window.confirm(
            'El DNI introducido no es válido. Pulsa confirmar para escribir el DNI de nuevo'
        );

        if (confirmBool) dniValue = parseInt(window.prompt('Escribe tu DNI de nuevo'));
    }


    function giveNum(dniValue) { return dniValue % 23; }

    function giveLetter(dniLetterNum) {
        let arrayLetter = [
            'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N',
            'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'
        ];

        return arrayLetter[dniLetterNum];
    }

    function giveAlert(dniValue, dniLetter) {
        window.alert(`La letra del DNI ${dniValue} es ${dniLetter}`);

    }


}

/** 5.
 *  Realizar estas 5 tareas con el mismo array:
 * 
 *      1.  Crear un array con estilos de música que contenga "Jazz" y "Blues"
 * 
 *      2.  Añadir "Rock-n-Roll" al final
 * 
 *      3.  Sustituir el valor central por Clásicos". El código para encontrar el
 *          elemento central debería funcionar para arrays con arrays con un numero
 *          impar de elementos.
 * 
 *      4.  Extrae el primer valor del array y muestralo.
 * 
 *      5.  Inserta al principio “Rap” y “Reggae” al array.
 * 
 */

function act5()
{
    // 1 -----------------------------------------------------------------------
    let estilosMusica = [
        "Jazz",
        "Blues"
    ];


    // 2 -----------------------------------------------------------------------
    estilosMusica.push("Rock-n-Roll");
    

    // 3 -----------------------------------------------------------------------
    let mitadArray = Math.floor((estilosMusica.length - 1) / 2);
    console.log(estilosMusica)
    console.log(`${mitadArray} -> ${estilosMusica[mitadArray]}`)


    // 4 -----------------------------------------------------------------------
    window.alert(estilosMusica[0]);

    
    // 5 -----------------------------------------------------------------------
    estilosMusica.unshift("Rap", "Reggae"); 
    console.log(estilosMusica)
}



/** 6.
 *  Crea un script que imprima 14 resultados aleatorios de una quiniela 1 X 2.
 *  
 *  Ejemplo de resultado:
 *      Resultado 1: 1
 *      Resultado 2: X
 *      Resultado 3: 2
 *      [...]
 *      Resultado 14: 2
 */

function act6()
{
    let resultados = ["1", "X", "2"];
    
    for (let i = 0; i < 14; i++) 
        document.writeln(`Resultado ${i}: ${resultados[Math.floor(Math.random() * 3)]}<br/>`);  
}



/** 7.
 *  Crea el script anterior pero con la probabilidad de que salga el 1 sea del
 *  60%, la de la X del 30% y la del 2 del 10%
 */

function act7()
{
    for (let i = 0; i < 14; i++) {
        let randomNum = Math.floor(Math.random() * 100);
        
        if      (randomNum <= 60) document.writeln(`Resultado ${i}: 1<br/>`);  
        else if (randomNum <= 90) document.writeln(`Resultado ${i}: X<br/>`);  
        else                      document.writeln(`Resultado ${i}: 2<br/>`);
    }
}



/** 8. 
 *  Crear una función que sume todos los elementos de un array y otra que a partir
 *  de un rango de valores minimo y maximo y un paso, devuelva un array con los
 *  elementos comprendidos dentro de dicho rango. El paso puede ser negativo también.
 * 
 *  Ejemplos
 *      console.log(range(1, 10))
 *      // → [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 * 
 *      console.log(range(5, 2, -1));
 *      // → [5, 4, 3, 2]
 * 
 *      console.log(sum(range(1, 10)));
 *      // → 55
 */

function act8() {
    let array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    const sumar = (...nums) => {
        let numSumado = 0;
        for (let num of nums) numSumado += num;
        return numSumado;
    }

    const rango = (min, max) => arr.slice(min, max);

    console.log(rango(1, 10))         // -> [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    console.log(rango(5, 2, -1));     // -> [5, 4, 3, 2]
    console.log(sumar(rango(1, 10))); // -> 55
}