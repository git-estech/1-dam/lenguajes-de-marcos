/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/


'use strict';


/*  1.
    Realiza un script que pida un texto y determine si esa cadena está formada sólo por mayúsculas, sólo por minúsculas o por una mezcla de ambas.
*/

function act1() {
    let texto = new String();
    texto = window.prompt('Introduce un texto para saber si usa solo mayúsculas, minúsculas, o ambas');


    if (texto != "") {

        switch (texto) {
            case texto.toUpperCase():
                window.alert('Esta cadena tiene solo mayúsculas');
                break;
                
            case texto.toLowerCase():
                window.alert('Esta cadena tiene solo minúsculas')
                break;
                    
            default:
                window.alert('Esta cadena tiene mayúsculas y minúsculas')
                break;
        }
    }
}



/*  2.
    Escribir una función ucFirst(str) que devuelva la cadena str con solo el primer caracter en mayúsculas, por ejemplo:
        ucFirst("john") == "John";
*/

function act2() {
    let stringFrase = new String();


    stringFrase = window.prompt('Escribe una palabra en minúscula')

    function ucFirst(stringFrase) {
        /*
            1. stringFrase.charAt(0).toUppercase: Cogemos solo el primer caracter y lo ponemos en mayúsculas
            2. stringFrase.slice(1): quitamos los caracteres !>= 1
        */
        return stringFrase.charAt(0).toUpperCase() + stringFrase.slice(1);
    }

    window.alert(ucFirst(stringFrase));

}



/*  3.
    Comprobar palabras spam: Escribe una función checkSpam(str) que devuelva true si str contiene la subcadena ‘viagra’ o ‘XXX’, en otro caso devuelve false.
*/

function act3() {
    let stringFrase = "";
    stringFrase = String(window.prompt('Comprobar palabras spam: devuelve bool true si escribes viagra o xxx'));

    function checkSpam(stringFrase) {
        // return (stringFrase === "xxx" || stringFrase === "viagra");
        return (stringFrase.includes("xxx") || stringFrase.includes("viagra")); // asi funciona con substrings
        
    }
        
    // window.alert(checkSpam(stringFrase));

    if (checkSpam(stringFrase)) {
        window.alert('El stringFrase escrito tiene una palabra spam');

    } else {
        window.alert('No se ha encontrado ninguna palabra spam');
        
    }

}



/*  4.
    Truncar textos: Crear una funcion truncate(str, maxlength) que compruebe la longitud de la cadena str y, si supera maxlength sustituya el final de la cadena str con el caracter elipsis"...", para hacer que tenga una longitud igual a maxlength.

    Ejemplos:
        truncate("Quiero decir algo acerca de este tema:", 20) = "Quiero decir algo a..."
        truncate("Hola a todos!", 20) = "Hola a todos!"
*/

function act4() {

    function truncate(stringFrase, maxLength) {
        if (stringFrase.length > maxLength) {
            let elipsis = "...";
            // return stringFrase = stringFrase.slice(0, maxLength-3) + "...";
            return stringFrase = stringFrase.slice(0, maxLength-elipsis.length) + elipsis;
        
        } else {
            return stringFrase;

        }
    }

    window.alert(truncate(window.prompt('Escribe frase larga a truncar y añadir elipsis'), 20));

}



/*  5.
    Realiza un script que pida cadenas de texto hasta que se pulse "cancelar". Al salir con "cancelar" deben mostrarse todas las cadenas concatenadas con un guión '-'.
*/

function act5() {
    let strOutput = new String(); 


    do {
        let str = window.prompt('Escribe una cadena de texto', 'palabra');
        
        if (str != null)
            if (!(strOutput == "")) strOutput += `-${str}`;
            else strOutput = str;

    } while (confirm('¿Quieres añadir otro?'));

    window.alert(`String: ${strOutput}`)

}



/*  6.
    Escribe un programa que pida una frase y escriba cuantas veces aparece la letra 'a'.
*/

function act6() {
    let str = new String();
    let cont;


    str  = window.prompt('Escribe una frase, y contaré las veces que salga la letra A')
    cont = str.split("a").length-1;

    window.alert(cont);

}



/*  7.
    Escribe un programa que pida una frase y escriba las vocales que aparecen.
*/

function act7() {
    let stringFrase   = "";
    let stringVocales = "";
    

    stringFrase = window.prompt('Escribe una frase para escribir las vocales que tiene');

    for (let i = 0; i < stringFrase.length; i++) {
        // window.alert(`stage 1: ${i}, ${stringFrase.length}`);

        switch (stringFrase.toLowerCase().charAt(i)) {
            case "a": case "e": case "i": case "o": case "u":
                // window.alert('stage 2: vowel');
                if (!(stringVocales.split(`${stringFrase.toLowerCase().charAt(i)}`) >= 1)) // no filtra bien
                    stringVocales += ` ${stringFrase.toLowerCase().charAt(i)},`;
                break;
                
            default:
                // window.alert('stage 2 no vowel');
                break;
        }
    }

    if (stringVocales != "") window.alert(`La frase ${stringFrase} usa las siguientes vocales:${stringVocales}`);

}



/*  8.
    Escribe un programa que pida una frase y escriba cuántas de las letras que tiene son vocales.
*/

function act8() {

}



/*  9.
    Escribe un programa que pida una frase y escriba cuántas veces aparecen cada una de las vocales.
*/

function act9() {

}



/*  10.
    Realiza un script que pida una cadena de texto y la muestre poniendo el signo – entre cada carácter sin usar el método replace.
    
    Por ejemplo, si tecleo “hola qué tal”, deberá salir “h-o-l-a- -q-u-e- -t-a-l”.
*/

function act10() {

}



/*  11.
    Pedimos una cadena de texto que sabemos que puede contener paréntesis. Realiza un script que extraiga la cadena que se encuentra entre los paréntesis.

    Ejemplo:
        Si escribimos el texto "Hola (que) tal" se mostrará "que". Si no existe el signo "(", mostrará una cadena vacía y si existe el signo "("pero no el signo")" mostrará desde el primer paréntesis hasta el final.
*/

function act11() {

}



/*  12.
    Realiza un script que pida una cadena de texto y la devuelva al revés. Es decir, si tecleo “hola que tal” deberá mostrar “lat euq aloh”. No usar ninguna función de strings para resolverlo.
*/

function act12() {

}



/*  13.
    Realiza un script que pida un texto e indique si es un palíndromo.
    
    Ejemplos:
        "Dábale arroz a la zorra el abad".
        "La ruta nos aporto otro paso natural".
*/

function act13() {

}



/*  14.
    Realiza un script que muestre la posición de la primera vocal de un texto introducido por teclado.
*/

function act14() {

}



/*  15.
    Realiza el mismo ejercicio anterior pero utilizando el método 'includes'.
*/

function act15() {

}