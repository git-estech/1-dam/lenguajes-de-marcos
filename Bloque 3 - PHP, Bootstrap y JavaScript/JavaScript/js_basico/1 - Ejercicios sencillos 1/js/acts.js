/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/


'use strict';


/*  1.
    Escribe un programa de una sola línea que haga que aparezca en la pantalla un alert que diga "Hello World".
*/

function helloWorldAlert() {
    window.alert('Hello World');

}



/*  2.
    Escribe un programa de una sola línea que escriba en la pantalla un texto que diga “Hello World” (document.write).
*/

function helloWorldDocWrite() {
    document.writeln('Hello World');

}



/*  3.
    Escribe un programa de una sola línea que escriba en la pantalla el resultado de sumar 3 + 5.
*/

function sumAndWrite() {   
    let num1, num2, numResult;
    num1 = 3;
    num2 = 5;
    numResult = num1 + num2;
    
    document.writeln(`${num1} + ${num2} = ${numResult}`);

}



/*  4.
    Escribe un programa de dos líneas que pida el nombre del usuario con un prompt y escriba un texto que diga “Hola nombreUsuario”
*/

function loginPrompt() {   
    let nombreUsuario = window.prompt('Escribe tu nombre');

    document.writeln(`Hola ${nombreUsuario}`);

}



/*  5.
    Escribe un programa de tres líneas que pida un número, pida otro número y escriba el resultado de sumar estos dos números.
*/

function sumAndWriteAgain() {
    let num1 = window.prompt('Escribe el primer número de la suma');
    let num2 = window.prompt('Escribe el segundo número de la suma');

    document.writeln(`La suma de ${num1} y ${num2} es ${parseInt(num1)+parseInt(num2)}`)

}



/*  6. 
    Escribe un programa que pida dos números y escriba en la pantalla cual es el mayor. 
*/

function whichNumbersBigger() {
    let num1 = window.prompt('Escribe el primer número para saber cual es mayor o menor de los dos');
    let num2 = window.prompt('Escribe el segundo número');

    if (num1 > num2) {
        document.writeln(`El número ${num1} es mayor que ${num2}`);

    } else if (num2 > num1) {
        document.writeln(`El número ${num2} es mayor que ${num1}`);

    } else if (parseFloat.num1 == parseFloat.num2) {
        // Seguro que lo ibas a intentar :P
        document.writeln(`El número ${num1} es igual que ${num2}`);

    } else {
        document.writeln('Enhorabuena, lo has roto');

    }

}



/*  7.
    Escribe un programa que pida 3 números y escriba en la pantalla el mayor de los tres.
*/

function whichNumbersBiggerAGAIN() {
    let num1 = window.prompt('Escribe el primer número para saber cual es mayor o menor de los tres');
    let num2 = window.prompt('Escribe el segundo número');
    let num3 = window.prompt('Escribe el tercer número');

    if (num1 > num2) {
        if (num1 > num3) {
            document.writeln(`El número ${num1} es mayor que ${num2} y ${num3}`);

        } else if (num3 > num1) {
            document.writeln(`El número ${num3} es mayor que ${num1} y ${num2}`);

        }

    } else if (num2 > num1) {
        if (num2 > num3) {
            document.writeln(`El número ${num2} es mayor que ${num1} y ${num3}`);

        } else if (num3 > num2) {
            document.writeln(`El número ${num3} es mayor que ${num2} y ${num1}`);

        }

    } else if (parseFloat(num1) == parseFloat(num2) && parseFloat(num2) == parseFloat(num3)) {
        // Seguro que lo ibas a intentar (de nuevo) :P
        document.writeln(`El número ${num1} es igual que ${num2} y ${num3}`);

    } else {
        document.writeln('Enhorabuena, lo has roto otra vez');

    }

}



/*  8.
    Escribe un programa que pida un número y diga si es divisible por 2.
*/

function divisibleByTwo() {
    let num = window.prompt('Escribe un número para saber si es divisible entre 2');
    
    if (num % 2 == 0) {
        window.alert(`El número ${num} es divisible entre 2`);

    } else {
        window.alert(`El número ${num} no es divisible entre 2`);

    }

}



/*  9.
    Escribe un programa que pida un número y nos diga si es divisible por 2, 3, 5 o 7 (sólo hay que comprobar si lo es por uno de los cuatro).
*/

function divisibleByWhatever() {
    let num = window.prompt('Escribe un número para saber si es divisible por 2, 3, 5 o 7');

    if (num % 2 == 0) {
        window.alert(`El número ${num} es divisible entre 2`);
        
    } else if (num % 3 == 0) {
        window.alert(`El número ${num} es divisible entre 3`);

    } else if (num % 5 == 0) {
        window.alert(`El número ${num} es divisible entre 5`);
            
    } else if (num % 7 == 0) {
        window.alert(`El número ${num} es divisible entre 7`);
    } 

}



/*  10.
    Añadir al ejercicio anterior que nos diga por cual de los cuatro es divisible (hay que decir todos por los que es divisible)
*/

function divisibleByWhateverAlt() {
    let num = window.prompt('Escribe un número para saber si es divisible por 2, 3, 5 o 7');

    var dvTwo, dvThree, dvFive, dvSeven;
    
    if (num % 2 == 0) { dvTwo   = true; }
    if (num % 3 == 0) { dvThree = true; }
    if (num % 5 == 0) { dvFive  = true; }
    if (num % 7 == 0) { dvSeven = true; }

    const resultStringTemplate = `El número ${num}`;
    var resultString = resultStringTemplate;


    if (!dvTwo && !dvThree && !dvFive && !dvSeven) {
        resultString = resultString + ` no es divisible por ningún número (2, 3, 5, 7)`;

    } else if (num == undefined || null) { // no funciona :(
        resultString = `El carácter introducido no es válido`;

    } else {
        resultString = resultString + ` es divisible por:`;

        if (dvTwo   == true) { resultString = resultString + ` 2,` }
        if (dvThree == true) { resultString = resultString + ` 3,` }
        if (dvFive  == true) { resultString = resultString + ` 5,` }
        if (dvSeven == true) { resultString = resultString + ` 7`  }
        
    }

    window.alert(resultString);

}



/*  11.
    Escribir un programa que escriba en pantalla los divisores de un número dado
*/

function iDontEvenKnow() {
    let numeroDado = window.prompt('Escribe un número');


    function doTheThing(numeroPasado) { // numero pasado siendo el numero que se le pasa cuando se le llama a la funcion

        const resultStringTemplate = `Divisores del número ${numeroPasado}:`;
        var resultString = resultStringTemplate;

        for (let i = numeroPasado; i != 0; i--) {
            if (numeroPasado % i == 0) {
                resultString = resultString + `\n${i},`;
                
            }
        }
        window.alert(resultString);
        
    }
    
    doTheThing(numeroDado);

}



/*  12.
    Escribir un programa que escriba en pantalla los divisores comunes de dos números dados
*/

function iDontEvenKnowAlt() {
    let numero1 = window.prompt('Escribe el primer número');
    let numero2 = window.prompt('Escribe el segundo número');
    

    function doTheThing(numeroPasado) { // numero pasado siendo el numero que se le pasa cuando se le llama a la funcion

        const resultStringTemplate = `Divisores del número ${numeroPasado}:`;
        var resultString = resultStringTemplate;

        for (let i = numeroPasado; i != 0; i--) {
            if (numeroPasado % i == 0) {
                resultString = resultString + `\n${i},`;
                
            }
        }
        window.alert(resultString);
        
    }
    
    doTheThing(numero1);
    doTheThing(numero2);

}



/*  13.
    Escribir un programa que nos diga si un número dado es primo (no es divisible por ninguno otro número que no sea él mismo o la unidad)
*/

function checkPrimeNumber() {
    let numero = window.prompt('Escribe un número');
    numero = parseInt(numero)
    var isPrime = true;
    
    if (numero > 1) {
        for (let i = numero-1; i >= 2; i--) {
            if (numero % i == 0) {
                isPrime = false;
                console.log(`${numero}, ${i}, ${isPrime}`);
            }
        }

    } else {
        let numero = window.prompt('Escribe un número válido (número positivo >= 1)');

    }


    if (isPrime) {
        window.alert(`El número ${numero} es primo`);

    } else {
        window.alert(`El número ${numero} no es primo`);

    }
    
}



/*  14.
    Pide la edad y si es mayor de 18 años indica que ya puede conducir.
*/

function isOver18() {
    let age = window.prompt('Escribe tu edad')

    if (age >= 18) {
        window.alert(`Tienes al menos 18 años (${age}) por lo que ya puedes conducir`)
    } else {
        window.alert(`No llegas a los 18 años (${age}) por lo que no puedes conducir`)
    }
    
}
