/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/

'use strict';




/*  1.
    Escribe el código que se indica, una línea para cada acción:
*/
function act1()
{
    //  1. Crea un objeto usuario vacío.
    let usuario = {};

    //  2. Agrega la propiedad 'nombre' con el valor Jose.
    usuario.nombre = 'Jose';

    //  3. Agrega la propiedad apellido con el valor Rodríguez.
    usuario.apellido = 'Rodriguez';
    window.alert(`${usuario.nombre}, ${usuario.apellido}`);

    //  4. Cambia el valor de nombre a Andrés.
    usuario.nombre = 'Andres';
    window.alert(`${usuario.nombre}, ${usuario.apellido}`);

    //  5. Comprobar que el objeto tiene una clave llamada 'apellido'.
    window.alert(`Clave apellido: ${usuario.hasOwnProperty('apellido')}`);

    //  6. Elimina la propiedad nombre del objeto y comprueba que ya no esta.
    delete usuario.nombre;
}




/*  2.
    Sumar propiedades de un objeto:
*/
function act2()
{
    //  Tenemos un objeto que almacena los salarios de nuestro equipo de
    //  trabajadores:

    let salarios = {
        John: 100,
        Ann : 160,
        Pete: 130
    }   // -> 360

    // let salarios = {
    //     John: 0,
    //     Ann : 0,
    //     Pete: 0
    // }   // -> 0

    //  Escribe el código para sumar todos los salarios y almacenar el resultado
    //  en la variable sum. En el ejemplo de arriba nos debería dar 390.
    //  Si salarios está vacío entonces el resultado será 0.

    let sum = 0;

    for (let propiedad in salarios) sum += salarios[propiedad];

    window.alert(sum);
}




/*  3.
    Crea una función multiplyNumeric(obj) que multiplique todas las propiedades
    numéricas de obj por 2.
*/
function act3()
{
    let output = object => window.alert(JSON.stringify(object));

    // Antes de la llamada
    let menu = {
        width: 200,
        height: 300,
        title: "Mi menú"
    };
    output(menu);

    let multiplyNumeric = obj =>
    {
        for (const propiedad in obj)
            if (typeof obj[propiedad] == 'number')
                obj[propiedad] *= 2;
    }

    multiplyNumeric(menu);
    output(menu);

    // Después de la llamada
    // menu = {
    //     width: 400,
    //     height: 600,
    //     title: "Mi menú"
    // };

    // Nota: multiplyNumeric no necesita devolver nada. Debe modificar el objeto original.
}




/*  4.
    Crea un objeto calculator con tres métodos:

    'read()' pide dos valores y los almacena como propiedades de objeto.
    'sum()' devuelve la suma de los valores almacenados.
    'mul()' multiplica los valores almacenados y devuelve el resultado.
*/
let calculator =
{
    read: () =>
    {
        this.num1 = +window.prompt('Introduce el número 1') || 0;
        this.num2 = +window.prompt('Introduce el número 2') || 0;
    },
    sum: () => this.num1 + this.num2,
    mul: () => this.num1 * this.num2
}

function act4()
{
    calculator.read();
    window.alert(`Suma: ${calculator.sum()}`);
    window.alert(`Multiplicación: ${calculator.mul()}`);
}




/*  5.
    Crear una función constructor Acumulador(valorInicial).
*/
function Acumulador(valorInicial)
{   // Las funciones 'constructor' no se pueden hacer con el formato fun flecha, parece?
    //  1. Almacene el “valor actual” en la propiedad value.
    //     El valor inicial se establece en el argumento del constructor valorInicial.
    this.value = valorInicial;

    //  El método read() debe usar prompt para leer un nuevo número y agregarlo
    //  a value.
    this.read = () => this.value += +window.prompt('Numero a escribir a value') || 0;

    //  En otras palabras, la propiedad value es la suma de todos los valores
    //  ingresados por el usuario con el valor inicial valorInicial.
}
function act5()
{
    let acumulador = new Acumulador(1); // valor inicial 1

    acumulador.read(); // agrega el valor introducido por el usuario
    acumulador.read(); // agrega el valor introducido por el usuario

    alert(acumulador.value); // muestra la suma de estos valores
}   //  ->  valor inicial (1) + x + y




/*  6.
    Ordenar por campo: Tenemos una serie de objetos para ordenar:
*/
let users = [
    { name: "John", age: 20, surname: "Johnson"  },
    { name: "Pete", age: 18, surname: "Peterson" },
    { name: "Ann",  age: 19, surname: "Hathaway" }
];
/*
    La forma habitual de hacerlo sería:

        por nombre(Ann, John, Pete)
            users.sort((a, b) => a.name > b.name ? 1 : -1);

        por edad (Pete, Ann, John)
            users.sort((a, b) => a.age > b.age ? 1 : -1);

        ¿Podemos hacerlo aún mas simple, como este?
            users.sort(byField('name'));
            users.sort(byField('age'));
 */
function act6()
{
    const byField = (campo) => (a, b) => a[campo] > b[campo] ? 1 : -1;

    window.alert( users.sort(byField('name')) );
    window.alert( users.sort(byField('age' )) );
}




/*  7.
    Tenemos un array de objetos user, cada uno tiene user.name. Escribe el
    código que lo convierta en un array de nombres.

    Por ejemplo:
        let john  = { name: "John", age: 25 };
        let pete  = { name: "Pete", age: 30 };
        let mary  = { name: "Mary", age: 28 };
        let users = [ john, pete, mary ];
        let names = ...tu código

        alert(names); // John, Pete, Mary
*/
function act7()
{
    //const valueToArray = array =>
}