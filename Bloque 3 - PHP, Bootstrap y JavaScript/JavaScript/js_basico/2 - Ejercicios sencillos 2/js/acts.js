/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/


'use strict';


/*  1.
    Reescribir el siguiente codigo usando una sentencia if..else que haga lo mismo:

    switch (browser) {
        case 'Edge':
            alert( "You've got the Edge!" );
            break;

        case 'Chrome':
        case 'Firefox':
        case 'Safari':
        case 'Opera':
            alert( 'Okay we support these browsers too' );
            break;

        default:
            alert( 'We hope that this page looks ok!' );
    }
*/

function act1() {
    
    // Constantes ya escritas para que sea más fácil ver si funciona,
    // moviendo con Alt las líneas para descomentarlas o no.
    const browser = 'Edge';
    /*
    const browser = 'Chrome';
    const browser = 'Firefox';
    const browser = 'Safari';
    const browser = 'Opera';
    */
    
    if (browser == 'Edge') {
        alert( "You've got the Edge!" );
    
    } else if (browser == 'Chrome' || browser == 'Firefox' || browser == 'Safari' || browser == 'Opera' ) {
        alert( 'Okay we support these browsers too' );

    } else {
        alert( 'We hope that this page looks ok!' );
        
    }

}



/*  2.
    Reescribir el siguiente codigo usando una única sentencia switch:

    let a = +prompt('a?', '');

    if (a == 0) {
        alert( 0 );
    }

    if (a == 1) {
        alert( 1 );
    }

    if (a == 2 || a == 3) {
        alert( '2,3' );
    }

*/

function act2() {
    let a = +prompt('a?', '');
    
    switch (a) {
        case 0:
            alert( 0 );
            break;
    
        case 1:
            alert( 1 );
            break;

        case 2: case 3:
            alert( '2,3' );
            break;

    }
}



/*  3.
    Pide una nota (número). Muestra la calificación según la nota:

    0-3: Muy deficiente
    3-5: Insuficiente
    5-6: Suficiente
    6-7: Bien
    7-9: Notable
    9-10: Sobresaliente

*/

function act3() {
    let nota = window.prompt('Escribe una nota');

    const plantillaFrase = `Con la nota ${nota} tienes un: `;
    var respuesta = plantillaFrase;
    var boolDoOutput = true;

    switch (parseInt(nota)) {
        case 0: case 1: case 2:
            respuesta = respuesta + `muy deficiente`;
            break;

        case 3: case 4:
            respuesta = respuesta + `insuficiente`;
            break;

        case 5:
            respuesta = respuesta + `suficiente`;
            break;

        case 6:
            respuesta = respuesta + `bien`;
            break;

        case 7: case 8:
            respuesta = respuesta + `notable`;
            break;

        case 9: case 10:
            respuesta = respuesta + `sobresaliente`;
            break;

        default:
            boolDoOutput = false;
            break;
    }


    if (boolDoOutput) {
        window.alert(respuesta);

    } else {
        window.alert(`El carácter introducido no es válido`);

    }

}



/*  4.
    Realiza un script que pida números hasta que se pulse “cancelar”. Si no es un número deberá indicarse con un “alert” y seguir pidiendo. Al salir con “cancelar” deberá indicarse la suma total de los números introducidos.

*/

function act4() {
    let numero = 0, resultado;
    
    while (true) {
        
        if (numero == null) {
            if (resultado != undefined) {
                window.alert(resultado);
                break;

            }
            
        } else {
            numero = Number(window.prompt('Escribe un número para sumar.'));
            resultado = resultado + numero;

        }
    } 
}



/*  5.
    Realiza un script que escriba una pirámide del 1 al 30 de la siguiente forma:

    1
    22
    333
    4444
    55555
*/

function act5() {
    let concatArray = new Array;

    // Iteramos del 1 al 30
    for (let i = 0; i < 31; i++) {
        concatArray[i] = ""; // parche para que no espamee undefined

        // y hasta que j == i, concatenamos i i veces
        for (let j = 0; j < i; j++) {

            // luego guardamos el valor en la posición [i] del array
            concatArray[i] = concatArray[i] + `${i}`;

        }   
    }

    // que imprimimos posteriormente
    for (let i = 0; i < concatArray.length; i++) {
        document.write(`${concatArray[i]}<br>`);
        
    }
}



/*  6.
    Haz un script que escriba una pirámide inversa de los números del 1 al número que indique el usuario
*/

function act6() {
    let numero = Number(window.prompt('Escribe el número máximo de la pirámide'));
    let concatArray = new Array;

    let j = Number(numero - 1) 

    for (let i = numero; i > 0; i--) {
        concatArray[i] = "";

        for (j; j > i; j--) {
            concatArray[i] = concatArray[i] + `${i}`;

        }   
    }


    for (let i = concatArray.length; i > 0; i--) {
        document.write(`${concatArray[i]}<br>`);
        
    }

}



/*  7.
    Crea script para generar pirámide siguiente con los números del 1 al número que indique el usuario (no mayor de 50):
*/