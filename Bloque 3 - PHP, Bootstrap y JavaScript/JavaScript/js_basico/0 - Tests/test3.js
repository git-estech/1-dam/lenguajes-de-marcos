'use strict';

let $numero1, $numero2, $resultado;

$numero1   = window.prompt('Introduce el primer numero');
$numero2   = window.prompt('Introduce el segundo numero');

$resultado = (parseFloat($numero1) + parseFloat($numero2));

if (isNaN($resultado) != true) {
    document.write('<h1>',$resultado,'</h1>');
} else {
    document.write('Error: valor(es) incorrectos');
}
