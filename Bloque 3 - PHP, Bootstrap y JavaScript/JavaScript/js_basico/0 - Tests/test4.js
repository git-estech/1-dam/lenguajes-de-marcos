let a = 1, b = 1;

let c = ++a; // 2, Incrementas a, asignas a -> c
let d = b++; // 1, Asignas b -> d, incrementas b

/*
the quick brown fox jumps over the lazy dog

the the the the the the the the the the the the the the the
quick quick quick quick quick quick quick quick quick quick
brown brown brown brown brown brown brown brown brown brown
fox fox fox fox fox fox fox fox fox fox fox fox fox fox fox
jumps jumps jumps jumps jumps jumps jumps jumps jumps jumps
over over over over over over over over over over over over
the the the the the the the the the the the the the the the
lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy lazy
dog dog dog dog dog dog dog dog dog dog dog dog dog dog dog
*/