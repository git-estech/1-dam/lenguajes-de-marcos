/*
    Manuel Jesús de la Fuente
    1o DAM -- es[tech]
*/


'use strict';


/*
    1.
    Crea una función que reciba 2 parámetros, precio e iva, y devuelva el precio
    con iva incluido. Si no recibe el iva, aplicará el 21 por ciento por defecto.
*/
function act1()
{
    let precioInput = parseFloat(window.prompt('Escribe el precio')) || 0;
    let ivaInput;
    
    let ivaInputParse = parseFloat(window.prompt('Escribe el iva')) || undefined;
    // lo había hecho antes con el nullish coalescending (??) pero no me fiaba
    // porque en MDN pone que es solo para null y undefined, y no para todos los
    // falsy como con || (que en este caso que da NaN creo que es mejor)

    // otra cosa es el uso de 'x || undefined' en vez de 'x || 21' para que use
    // el valor por defecto de la fun. no se si es la mejor forma

    let act1Fun = (precio, iva = 21) => ((precio * iva) / 100) + precio;

    document.write(act1Fun(precioInput, ivaInput));
}



/*
    2.
    Crea una función que genere número entero aleatorio entre min y max, que
    serán pasados como parámetros. Por defecto min = 1 y max = 100.
*/
function act2()
{
    let numMin = parseFloat(window.prompt('Escribe el número mínimo')) || undefined;
    let numMax = parseFloat(window.prompt('Escribe el número máximo')) || undefined;

    let randomNum = (min = 1, max = 100) => Math.floor(Math.random() * (max - min + 1) + min);

    document.write(randomNum(numMin, numMax));
}



/*
    3.
    Crea una función que genere 100 números aleatorios entre 1 y 1000 que no se
    repitan y luego muéstralos por pantalla.
*/
function act3()
{
    let randomNum = 0;
    const min = 1, max = 1000;

    const numAmount = 100;
    let arr = [];
    let isRepeated = false;

    for (let i = 0; i < numAmount; i++) {
        do {
            randomNum = Math.floor(Math.random() * (max - min + 1) + min);
            isRepeated = false;

            isRepeated = arr.includes(randomNum);
        } while (isRepeated);

        arr[i] = randomNum;
    }
    
    arr.forEach(num => { document.writeln(`${num}<br/>`); });
}



/*
    4.
    Filtrar array según un rango maximo y minimo de valores: Escribir una funcion
    llamada filtrarRango(arr, a, b) que a partir del array "arr", busque elementos
    con valor mayor o igual a "a" y menores o iguales a "b" y los devuelva como
    un array.

    La función no debería modificar el array original, debe devolver otro nuevo.
*/
function act4()
{
    const arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];

    let min = parseInt(window.prompt('Escribe el número mínimo. Rango: [0...9]'));
    let max = parseInt(window.prompt('Escribe el número máximo. Rango: [0...9]'));

    function filtrarRango(arr, a, b)
    {   return arr.filter(numero => numero >= a && numero <= b)
    }

    // let filtrarRango = (arr, a, b) => arr.filter(numero => numero >= a && numero <= b)
    // usaría este pero el enunciado pone "una funcion llamada filtrarRango(arr, a, b)"

    document.write(filtrarRango(arr, min, max));
}



/*
    5.
    Eliminar valores fuera de rango: Escribir una función llamada
    filtrarRangoEnPosicion(arr, a, b) que a partir del array "arr", elimine de el
    todos los valores que no esten entre "a" y "b".

    La comprobación que hay que hacer es: a ≤ arr[i] ≤ b.

    La función debe modificar solo el array original. No debe devolver nada.
*/
function act5()
{
    let arr = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ];

    let min = parseInt(window.prompt('Escribe el número mínimo. Rango: [0...9]'));
    let max = parseInt(window.prompt('Escribe el número máximo. Rango: [0...9]'));

    let filtrarRangoEnPosicion = (arr, a, b) => {
        arr.splice(0, a).splice(arr.length, -(arr.length - b) ) // el segundo splice del max no funciona :(
        return arr // arr.splice devuelve lo que se ha borrado en otro segundo arr
                   // por lo que hay que hacer un return explicito del arr normal
    }

    // seguro que asi no es como se hace, verdad?

    document.write(filtrarRangoEnPosicion(arr, min, max));
}



/*
    6.
    Filtrar valores únicos: Escribir una función que devuelva los valores únicos
    de un array, es decir que no esten repetidos.

    Ej: dado [a,b,b,d,e,c,d,c] devuelve [a,b,d,e,c]
*/
function act6()
{
    let arr = ['a', 'b', 'b', 'd', 'e', 'c', 'd', 'c'];

    let arrDedup = arr => [...new Set(arr)];
    // con new Set deduplico los valores y con el spread (...) troceo el set nuevo
    // no estoy convencido al 100% si lo correcto aqui es usar un new...
    // según he leido es muy pesado para el navegador. pero funcionar funciona

    window.alert(arrDedup(arr));
}



/*
    7.
    Ya conocemos el método Array.filter(f). Filtra todos los elementos usando la
    función f. Si ésta devuelve true, entonces ese elemento se incluye en el array
    devuelto.

    Vamos a hacer estos 2 filtros:
        inBetween(a, b) – between a and b or equal to them (inclusively).
        inArray([...]) – in the given array.

    Se deben usar de esta forma:
        arr.filter(inBetween(3,6))   – selecciona los valores que esten solo entre 3 y 6.
        arr.filter(inArray([1,2,3])) – selecciona los elementos que coincidan con
                                       alguno de los valores en [1,2,3].
    Por ejemplo:
        let arr = [1, 2, 3, 4, 5, 6, 7];
        alert( arr.filter(inBetween(3, 6)) ); // 3,4,5,6
        alert( arr.filter(inArray([1, 2, 10])) ); // 1,2
*/
function act7()
{
    let arr = [1, 2, 3, 4, 5, 6, 7];

    let inBetween = (a, b) => (num) => num >= a && num <= b
    let inArray   = (arr)  => (num) => arr.includes(num)

    window.alert( arr.filter(inBetween(3, 6)) );     // 3, 4, 5, 6
    window.alert( arr.filter(inArray([1, 2, 10])) ); // 1, 2
}



/*
    8.
    Aplanar arrays: use el método reduce en combinación con el método concat para
    “aplanar” un array de arrays en un único array que tenga todos los elementos
    de los arrays originales.

    Por ejemplo:
        let arrays = [[1, 2, 3], [4, 5], [6]];
        salida: [1, 2, 3, 4, 5, 6]
*/
function act8()
{
    let arrays = [[1, 2, 3], [4, 5], [6]];
    window.alert( arrays.reduce((ant, post) => ant.concat(post)) );
}



/*
    9.
    Ordenar en orden decreciente: usando una función flecha

    let arr = [5, 2, 1, -10, 8];
    // ... tu código para ordenar en orden decreciente
    alert( arr ); // 8, 5, 2, 1, -10
*/
function act9()
{
    let arr = [5, 2, 1, -10, 8];

    arr.sort( (ant, post) => post - ant );

    alert( arr ); // 8, 5, 2, 1, -10
}
