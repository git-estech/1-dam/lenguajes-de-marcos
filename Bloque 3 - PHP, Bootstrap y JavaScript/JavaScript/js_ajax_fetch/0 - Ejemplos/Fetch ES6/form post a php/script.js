const $btnPeticion = document.querySelector("#btnPeticion");

$btnPeticion.addEventListener("click", (e) => {
const data = new FormData(document.getElementById('formulario'));
console.log(data);
	e.preventDefault();
	fetch('post.php', {
	   method: 'POST',
	   body: data
	})
	.then(function(response) {
	   if(response.ok) {
		   return response.text()
	   } else {
		   throw "Error en la llamada Ajax";
	   }
	})
	.then(function(texto) {
		console.log(texto);
	})
	.catch(function(err) {
		alert(err);
		console.log(err);
	});
});