const $btnPeticion = document.querySelector("#btnPeticion");

$btnPeticion.addEventListener("click", () => {
	fetch('datos.txt', {
	   method: 'GET',
	})
	.then(function(response) {
		//console.dir(response);
	   if(response.ok) {
		   return response.text()
	   } else {
		   throw "Error en la llamada Ajax";
	   }
	})
	.then(function(texto) {
		console.log(texto);
	})
	.catch(function(err) {
		console.log(err);
	});
});