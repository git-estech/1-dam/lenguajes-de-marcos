module.exports = {
    //php: '/usr/bin/php', // Linux/macOS (example)
    php: "C:\\laragon\\bin\\php\\php-7.4.19-Win32-vc15-x64\\php.exe", // Windows, es[tech]
    // -> C:\laragon\bin\php\php-7.4.19-Win32-vc15-x64\php.exe

    // Externally accesible ::::::::::::::::::::::::::::::::::::::::::::::::
    host: '0.0.0.0', // default: '0.0.0.0' (could also be 'localhost' for example)
    remoteLogs: 'magenta' // true | false | Color
    //useLocalIp: true, // optional: opens browser with your local IP
}