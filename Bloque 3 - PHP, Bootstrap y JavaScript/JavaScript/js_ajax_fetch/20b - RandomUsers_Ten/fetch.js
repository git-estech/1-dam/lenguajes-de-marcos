"use strict"

const contenido = document.querySelector("#contenido")

const base_url = "https://randomuser.me/api/"
const endpoint = ""
const queries = "?results=10"

let get_content = () =>
{ // async fetch
	fetch(base_url + endpoint + queries, {
		method: "GET",
		//headers: añadir una key de la api en el caso de que lo pida
		//"Content-Type": "application/json"
	})
	.then(response => {
		if (response.ok)
			return response;
		else throw "Error";
	})
	.then(data => data.json())
	.then(data => set_content(data))
	.catch((err) => {
		console.log(err)
	})
}

let set_content = (data) =>
{
	data.results.forEach((person) => {

		let card = document.createElement("div")
		card.classList.add("card")
		let card_content = document.createElement("div")
		card_content.classList.add("card_content")

		let img = document.createElement("img")
		img.classList.add("image")

		let name = document.createElement("h1")
		name.classList.add("name")

		let email = document.createElement("p")
		email.classList.add("email")

		let fullname = `${person.name.title} ${person.name.first} ${person.name.last}`

		img.src         = person.picture.large
		name.innerText  = fullname
		email.innerText = person.email

		card.appendChild(img);
		card_content.appendChild(name);
		card_content.appendChild(email);
		card.appendChild(card_content)

		contenido.appendChild(card);
	})
}

window.onload = () => get_content()