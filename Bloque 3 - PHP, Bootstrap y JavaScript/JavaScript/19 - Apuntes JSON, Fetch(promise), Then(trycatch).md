# Como procesar JSON con parse y stringify

https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/JSON#converting_between_objects_and_text

```js
	parse(): acepta json, devuelve objeto JS
	stringify(): acepta objecto js, devuelve string json
		undefined, funciones etc pasa a ser null

// String con formato JSON
const json = '{ "nombre": "pepito", "edad": 20 }'

// De string formato json a objeto de JS
const parsedJson = JSON.parse(json)

// y de objeto de JS a string formato JSON
JSON.stringify(parsedJson) // -> { "nombre": "pepito", "edad": 20 }
```




# Usando el objeto Promise para hacer peticiones asíncronas

https://developer.mozilla.org/en-US/docs/Web/API/Response

Constructor del objeto: https://developer.mozilla.org/en-US/docs/Web/API/Response/Response

```js
	new Promise(/*ejecutor*/ function(resolve, reject) {...})


let promiseName = new Promise((resolve, reject) =>
{
	try {
		setTimeout(function()
		{ // time limited function
			// code to be executed...
			resolve("Success!"); // mark function as resolved (successful)
			return;
		}, 250) // time limit

	} catch(err) {
	  // if after the delay it wasn't resolved (successful)
		// then use reject (failure) to spit out an error
		//reject(`Error: ${err}`);
		reject(err);
	}
})


promiseName.then((successMessage) => // successMessage es el string de dentro de resolve()
{ // if the promise is resolve()d, then execute this code
	console.log("Yay! " + successMessage)

}).catch(function(err)
{
	console.log("Error :(" + err)
});
```


```js
getData(url)
	.then(fetchData) // "then" returns a promise
	.then(getFetchData)
	.then(getFetchGetData)
	.catch(function(error)
	{
		console.log("Error: " + error)
	})
```

## Multiple .then()s

```js
function runAnimation(position) 
{
	console.log("Moving to position..." + position);
}

function delay(interval)
{
	return new Promise(function(resolve)
	{
		setTimeout(resolve, interval);
	});
}

runAnimation(0);
delay(1000)
	.then(function()
	{ // first, do this.
		runAnimation(1);
		return delay(1000);
		// success ? next .then() : .catch(...)
	})
	.then(function()
	{ 
		runAnimation(2);
	});
	.catch(function(error)
	{ // code to be executed when any of the .then() fails
		console.log("Error :" + error);
	})
```

```js
// function getRandomTimeout()
// 	{ return Math.floor(Math.random() * (1 - 5) + 5) * 1000; }
let getRandomTimeout = () => Math.floor(Math.random() * (1 - 5) + 5) * 1000

function animation(position)
{
	console.log("Moving to position..." + position);
	return new Promise(function(resolve)
	{
		setTimeout(resolve, getRandomTimeout());
	})
}

Promise.all([
	animation(1),
	animation(2),
	animation(3),
	animation(4),
]).then(function()
{
	animation(5)
})
```


# Fetch API

```js
fetch("http//example.com/movies.json")
	.then(response => response.json())
	.then(data => console.log(data))
```


## GET
```js <!-- https://parzibyte.me/blog/2019/06/03/fetch-api-javascript-peticiones-http-promesas/ -->

const btnPeticion = document.querySelector("#btnPeticion")

btnPeticion.addEventListener("click", e => {
	fetch('datos.php', {
		method: 'GET',
	})
	.then(function(response)
	{
		if (response.ok)
			return response.text()
		else
			throw "error llamada ajax";
	})
	.then(function(texto)
	{
		console.log(texto);
	})
	.catch(function(err)
	{
		console.log(err);
	});
})
```

## POST
```js
const btnPeticion = document.querySelector("#btnPeticion"),

btnPeticion.addEventListener("click", (e) =>
{
	const data = new FormData(document.getElementById('formulario'));
	console.dir(data);

	e.preventDefault();
	fetch('post.php'. {
		method: POST,
		body: data
	})
	.then(function(response)
	{
		if (response.ok)
			return response.text()
		else
			throw "error llamada ajax";
	})
	.then(function(texto)
	{
		console.log(texto);
	})
	.catch(function(err)
	{
		console.log(err);
	});
});
```
```php
<?php @_POST['nombres']

```