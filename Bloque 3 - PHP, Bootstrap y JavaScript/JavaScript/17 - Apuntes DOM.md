# DOM examples
[TOC]

From https://www.w3schools.com/js/js_dom_examples.asp

---
<br>

## The document object
<br>

### Cookies: display name/value pair of document
<details>
  <summary>document.cookie</summary>

  ```html
  <p id="demo">Click the button to display the cookies associated with this document.</p>
  <button onclick="getCookie()">Try it</button>
  ```
  ```js
  function getCookie()
  {
      document.getElementById("demo").innerHTML =
      "Cookies associated with this document: " + document.cookie;
  }
  ```
</details>
<br>

### Domain name of server
<details>
  <summary>document.domain</summary>

  ```html
  <p id="demo">Click the button to display the cookies associated with this document.</p>
  <button onclick="getDomain()">Try it</button>
  ```
  ```js
  document.getElementById("demo").innerHTML = document.domain;
  ```
</details>
<br>

### Timestamp of last change
<details>
  <summary>document.lastModified</summary>

  ```js
  document.getElementById("demo").innerHTML = document.lastModified;
  ```
</details>
<br>

### Title of document
<details>
  <summary>document.title</summary>

  ```js
  document.getElementById("demo").innerHTML = document.title;
  ```
</details>
<br>

### URL of document
<details>
  <summary>document.URL</summary>

  ```js
  document.getElementById("demo").innerHTML = document.URL
  ```
</details>
<br>

### Replace content of document
<details>
  <summary>document.URL</summary>

  ```js
  function replaceContent()
  {
      document.open("text/html","replace");
      document.write("<h2>Learning about the HTML DOM is fun!</h2>");
      document.close();
  }
  ```
</details>
<br>

### Open new window and add content
<details>
  <summary>window.open()</summary>

  ```js
  window.open().document.open();
  window.open().document.write("<h2>Hello World!</h2>");
  window.open().document.close();
  ```
</details>
<br>

### Display number of elements with a specific name
<details>
  <summary>document.getElementsByName("x")</summary>
  
  ```html
  <p>
    Cats: <input name="x" type="radio" value="Cats">
    Dogs: <input name="x" type="radio" value="Dogs">
  </p>
  <p>
    <input type="button" onclick="getElements()" value="How many elements named x?">
  </p>
  <p id="demo"></p>
  ```
  ```js
  function getElements()
  {
      var x = document.getElementsByName("x");
      document.getElementById("demo").innerHTML = x.length;
  }
  ```
</details>
<br>

### Display number of elements with a specific tag name
<details>
  <summary>document.getElementsByTagName("input");</summary>
  
  ```html
  <input type="text" size="20"><br>
  <input type="text" size="20"><br>
  <input type="text" size="20"><br>
  ```
  ```js
  function getElements()
  {
      var x = document.getElementsByTagName("input");
      document.getElementById("demo").innerHTML = x.length;
  }
  ```
</details>
<br>

---
<br>

## CSS manipulation
<br>

### Change the visibility of an HTML element
<details>
  <summary>.style.visibility='hidden' || .style.visibility='visible'</summary>

  ```html <!-- with inline js :( -->
  <p id="hey">hello, world!</p>

  <input type="button" value="Hide text"
  onclick="document.getElementById('hey').style.visibility='hidden'">
  
  <input type="button" value="Show text"
  onclick="document.getElementById('hey').style.visibility='visible'">
  ```
</details>
<br>

### Change the background color of an HTML element
<details>
  <summary>.style.background</summary>

  ```html
  <table style="width:300px;height:100px">
    <tr>
      <td onmouseover="bgChange(this.style.backgroundColor)" 
          onmouseout="bgChange('transparent')"
          style="background-color:Khaki">
      </td>
      <td onmouseover="bgChange(this.style.backgroundColor)" 
          onmouseout="bgChange('transparent')"
          style="background-color:PaleGreen">
      </td>
      <td onmouseover="bgChange(this.style.backgroundColor)" 
          onmouseout="bgChange('transparent')"
          style="background-color:Silver">
      </td>
    </tr>
  </table>
  ```
  ```js
  function bgChange(bg)
  {
      document.body.style.background = bg;
  }
  ```
</details>
<br>

---
<br>

## Misc
<br>

### Get Href from first link in document
```html
<p>
  <a href="/html/default.asp">HTML</a>
<br>
  <a href="/css/default.asp">CSS</a>
</p>

<p id="demo"></p>
```
```js
document.getElementById("demo").innerHTML =
"The href of the first link is " + document.links[0].href;
```
<br>

### Find name from first form in document
```html
<form name="Form1"></form>
<form name="Form2"></form>
<form name="Form3"></form>

<p id="demo"></p>
```
```js
document.getElementById("demo").innerHTML =
"The name of the first for is " + document.forms[0].name;
```
<br>

## Id from first image
```html
<img id="img1" src="pic_htmltree.gif">
<img id="img2" src="pic_navigate.gif">

<p id="demo"></p>
```
```js
document.getElementById("demo").innerHTML =
"The id of the first image is " + document.images[0].id;
```