"use strict"

/*
 * Estructura HTML del ejemplo
 *
 * <body>
 * 	<div class="grandparent" id="grandparent-id">
 * 		<div class="parent">
 * 			<div class="child"></div>
 * 			<div class="child"></div>
 * 		</div>
 * 		<div class="parent">
 * 			<div class="child"></div>
 * 			<div class="child"></div>
 * 		</div>
 * 	</div>
 * </body>
 */

let changeColor = element => element.style.backgroundColor = "#333"


// getElementById
const grandparent = document.getElementById("grandparent-id")
changeColor(grandparent)

// getElementsByClassName
// const parents = document.getElementsByClassName("parent") // HTMLCollection
const parents = Array.from(document.getElementsByClassName("parent")) // Array
parents.forEach(changeColor) // el array tiene foreach pero HTMLCollection no

// querySelector (sirve para todos los tipos)
// Solo coge el primero que encuentra en el DOM
const grandparent2 = document.querySelector("#grandparent")
const parent2      = document.querySelector(".parent")

// querySelectorAll
const parents2 = document.querySelectorAll(".parent")
parents2.forEach(changeColor)




// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


// Select children element (go down in hierarchy)
const grandparent = document.querySelector(".grandparent")
const parents     = Array.from(grandparent.children)
parents.forEach(changeColor)

const parentOne = parents[0]
const children = parentOne.children
changeColor(children[0])


// Select descendant (go down in hierarchy starting from parent)
const childrenTwo = grandparent.querySelectorAll(".child") // descendiente de grandparent
childrenTwo.forEach(changeColor)


// Select parent
const childOne       = document.querySelector("#child-one")
const parent         = childOne.parentElement
const grandparentTwo = parent.parentElement


// Select ancestors (first match going up)
const grandparent3 = childOne.closest(".grandparent")

// Select sibling
const childTwo = childOne.nextElementSibling
const childOne_again = childTwo.previousElementSibling
