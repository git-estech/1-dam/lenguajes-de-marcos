"use strict"

const grandparent = document.querySelector(".grandparent")
const parent = document.querySelector(".parent")
const child = document.querySelector(".child")




grandparent.addEventListener("click", e => {
	console.log(e) // devuelve un objeto con la informacion del click, coordenadas etc
	console.log(e.target)
})
// .addEventListener(tipo de evento, función callback a ejecutar, )




/*
 * Event bubbling
 *
 * Si un div con un EventListener está detrás de otro div con otro EventListener
 * se detectará también el evento y se ejecutará por orden child > parent > grandparent
 */

grandparent.addEventListener("click", e => {
	console.log("Grandparent Bubble")
})


parent.addEventListener("click", e => {
	// e.stopPropagation() // hace que pare el bubbling o capture
	console.log("Parent Bubble")
})

child.addEventListener("click", e => {
	console.log("Child Bubble")
})

// -> "Child 1" \n "Parent 1"




/*
 * Event capturing
 *
 * Como bubbling, pero se invierte el orden a grandparent > parent > child.
 *
 * Primero se escanean los eventos con capture, y luego con bubbling.
 * Esto significa que se puede combinar aqui para que el orden sea
 * grandparent(capture) > child(bubbling) > parent(bubbling)
 */

grandparent.addEventListener("click", e => {
	console.log("Grandparent Capture")
}), {capture: true}




/*
 * Ejecutar funcion solo una vez
 */

parent.addEventListener("click", e => {
	//...código
}), {once: true}
	// o parent.removeEventListener (solo funciona al llamar a una función externa)




/*
 * Delegación de eventos
 */

const divs = document.querySelectorAll("divs")
divs.forEach(div => {
	div.addEventListener("click", () => {
		console.log("hi")
	})
}) // añade un event listener de click a todos los divs

// este div se crea después por lo que no lo tiene
const newDiv = document.createElement("div")
newDiv.style.width  = "200px"
newDiv.style.height = "200px"
newDiv.style.backgroundColor = "purple"
document.body.append(newDiv)

// entonces, ¿como hacemos que funcione con todos los divs, incluyendo este ultimo?
// con e.target.matches("[tag]")
document.addEventListener("click", e => {
	if (e.target.matches("div")) {
		console.log("hi")
	}
})

// ejemplo con una funcion propia:
function addGlobalEventListener(type, selector, callback) {
	document.addEventListener(type, e => {
		if (e.target.matches(selector)) callback(e)
	})
}
addGlobalEventListener("click", "div", e => {
	console.log("hey")
})
