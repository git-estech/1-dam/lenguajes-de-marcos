document.getElementById("id-elemento")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById

document.getElementsByClassName("parent") // -> NodeList, usar Array.from(document.getEl...()) parar usar forEach
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementsByClassName

document.querySelector("div") // Primer match
document.querySelector("#id")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelector

document.querySelectorAll("span") // -> NodeList, usar Array.from(document.getEl...()) parar usar forEach
document.querySelectorAll(".class")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelectorAll



const grandparent    = document.querySelector(".grandparent")
const parents        = Array.from(grandparent.children)
const parentOne      = parents[0]
const children       = parentOne.children
changeColor(children[0])



