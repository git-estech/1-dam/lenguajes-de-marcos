/* Ejemplo incompleto para hacerte una idea de donde salen los elementos
 * <body>
 * 	<div id="error"><div>
 * 	<form id="form" action="/" method="GET">
 * 		<div>
 * 			<label for="name">Name</label>
 * 			<input id="name" name="name" type="text">
 * 		</div>
 * 		<div>
 * 			<label for="password">Password</label>
 * 			<input id="password" name="password" type="password">
 * 		</div>
 * 		<button type="submit">Submit</button>
 * 	</form>
 * </body>
 *
 *  *** TIP: <head><script defer src="./script.js"></script></head> ***
 * 	esto hace que aunque se ponga en el head, espere a que el dom esté
 * 	listo antes de ejecutar nada
 *
 *  *** TIP: Un input se puede marcar como required desde el HTML con required
 * 	<input id="password" name="password" type="password" required>
 */


"use strict"

const name         = document.getElementById("name");
const password     = document.getElementById("password");
const password2    = document.getElementById("password2");

const form         = document.getElementById("form");
const errorElement = document.getElementById('error');


// al hacer click en submit...
form.addEventListener("submit", e => {
	// arr de mensajes de error
	let messages = []

	if (name.value === "" || name.value == null) {
		messages.push("name is required")
	}

	if (password.length <= 6) {
		messages.push("Password must be longer than 6 characters")
	}

	if (messages.length > 0) { // si el arr de errores tiene un err
		e.preventDefault() // ...evitar que se pueda enviar
		errorElement.innerText = messages.join(", ")
	}


})




// !!! otra forma de hacerlo ::::::::::::::::::::::::::::::::::::::::::::::::::
// https://www.youtube.com/watch?v=CYlNJpltjMM

/*
<form id="form" action="/">
	<h1>Registration</h1>
	<div class="input-control">
		<label for="username">Username</label>
		<input id="username" name="username" type="text">
		<div class="error"></div>
	</div>
	<div class="input-control">
		<label for="email">Email</label>
		<input id="email" name="email" type="text">
		<div class="error"></div>
	</div>
	<div class="input-control">
		<label for="password">Password</label>
		<input id="password"name="password" type="password">
		<div class="error"></div>
	</div>
	<div class="input-control">
		<label for="password2">Password again</label>
		<input id="password2"name="password2" type="password">
		<div class="error"></div>
	</div>
	<button type="submit">Sign Up</button>
</form>
*/
form.addEventListener("submit", e => {
	e.preventDefault();
	validateInputs();
});

const validateInputs = () => {
	const usernameValue  = username.value.trim();
	const emailValue     = email.value.trim();
	const passwordValue  = password.value.trim();
	const passwordValue2 = password2.value.trim();

	// username
	if (username.value === "" || username.value == null)
		setError(username, "Name is required");
	else setSuccess(username);

	// email
	if (email.value === "")
		setError(email, "Email is required");
	else if (!isValidEmail(emailValue))
		setError(email, "Provide a valid email address");
	else setSuccess(email);

	// password
	if (passwordValue === "")
		setError(password, "Password is required");
	else if (passwordValue.length < 8)
		setError(password, "Password must at least be 8 characters");
	else setSuccess(password);

	// password2
	if (passwordValue2 === "")
		setError(password2, "Please confirm your password");
	else if (passwordValue2 !== passwordValue)
		setError(password2, "Passwords don't match");
	else setSuccess(password2);
}

const isValidEmail = email => {
	const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regexp.test(String(email).toLowerCase());
}

const setError = (element, message) => {
	const inputControl = element.parentElement;
	const errorDisplay = inputControl.querySelector(".error");

	errorDisplay.innerText = message;
	inputControl.classList.add("error");
	inputControl.classList.remove("success")
}

const setSuccess = element => {
	const inputControl = element.parentElement;
	const errorDisplay = inputControl.querySelector(".error");
}
