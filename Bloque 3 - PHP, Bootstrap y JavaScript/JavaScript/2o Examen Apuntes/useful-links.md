# Selectores de nodos

```js
// ID
document.getElementById("id-elemento")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById

// Class
document.getElementsByClassName("parent") // -> NodeList, usar Array.from(...) parar usar forEach
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementsByClassName

// Tags
document.querySelector("div") // Primer match
document.querySelector("#id")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelector

document.querySelectorAll("span") // -> NodeList, usar Array.from(...) parar usar forEach
document.querySelectorAll(".class")
        // -> https://developer.mozilla.org/en-US/docs/Web/API/Element/querySelectorAll


const grandparent    = document.querySelector(".grandparent")
const parents        = Array.from(grandparent.children)
const parentOne      = parents[0]
const children       = parentOne.children
changeColor(children[0])

const parent         = childOne.parentElement
const grandparent3   = childOne.closest(".grandparent")

const childOne_again = childTwo.previousElementSibling
const childTwo       = childOne.nextElementSibling

let changeColor = element => element.style.backgroundColor = "#333"
```

https://developer.mozilla.org/en-US/docs/Web/API/Document_object_model/Locating_DOM_elements_using_selectors#the_nodeselector_interface


```js
inputControl.classList.add("error");
inputControl.classList.remove("success")
```




# Crear y modificar nodos
```js
const body = document.body

// añadir elemento
body.appendChild() // solo se pueden pasar <elementos> html
body.append("foo", "bar") // con append, también se puede varios <elementos> o "strings" 

// crear elemento
const div = document.createElement("div") // crear un div del lado de js
body.append(div) // añadir este div al body

// *** crear elemento 2
const strong = document.createElement("strong") // element = <tag> html
strong.innerText = "Hola Mundo"
div.append(strong)

// modificar elemento
div.innerText   = "Foo"
div.textContent = "Bar"
div.innerHTML   = "<p>Párrafo</p>"

// borrar elemento
const spanBye = document.querySelector("#bye")
spanBye.remove()    // borrar
div.append(spanBye) // volverlo a añadir

// modificar atributo (id, class etc) de los elementos
// <span id="hi" title="hello">
spanHi.getAttribute("id") // -> hi
spanHi.getAttribute("title") // -> hello

spanHi.setAttribute("id", "id_value") // -> // <span id="id_value" title="hello">
spanHi.removeAttribute("id") // -> <span title="hello">

spanHi.classList.add("new-class")
spanHi.classList.remove("new-class")
spanHi.classList.toggle("hidden") // borrar si está, añadir si no

// <span data-test="texto"></span>
spanHi.dataset // domstringmap de todos los datasets
spanHi.dataset.test // -> "texto" // acceder dataset en concreto
spanHi.dataset.newName = "new-name" // nuevo dataset
```




# Eventos (event listener)

```js
let button1 = document.getElementById("button-ok");

// via bubbling
button1.addEventListener("click", e => { // 'e' es un objeto con cosas dentro // button1.removeElementListener desde una funcion externa
    e.stopPropagation() // para parar el bubbling/capture
    // código al hacer click
}), /* vacío, {once: true} o {capture: true} para usar capture en vez de bubbling */

// a todos los matches de la página
let addGlobalEventListener = (tipo, selector, callback) => {
    document.addEventListener(tipo, e => {
        if (e.target.matches(selector)) callback(e)
    });
}
addGlobalEventListener("click", "div", e => {
    console.log("hey")
});
```

https://developer.mozilla.org/en-US/docs/Web/API/EventTarget
https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener




# CSS

```js
const newDiv = document.createElement("div")
newDiv.style.width = "200px"
newDiv.style.height = "200px"
newDiv.style.backgroundColor = "purple" // css-tag to camelCaseTag

document.body.append(newDiv)


spanHi.style.color = "blue"
spanHi.style.backgroundColor = "yellow"
```




# Regex / RegExp

```js
const email      = document.getElementById("email");
const emailValue = email.value.trim();

form.addEventListener("submit", e => {
	e.preventDefault();
	validateInputs();
});


const validateInputs = () => {
    if (!isValidEmail(emailValue)) { /* error */ }
}

const isValidEmail = email => {
    const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regexp.test(String(email).toLowerCase())
}

form.addEventListener("submit", e => {
	e.preventDefault();
	validateInputs();
});
```

https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test