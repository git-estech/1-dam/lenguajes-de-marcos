"use strict"

/*
 * Añadir elementos a la página
 */

const body = document.body

body.appendChild() // Con appendChild solo puedes pasar elementos HTML
body.append("Hello World", "Bye") // Con append puedes pasar elementos HTML, o varios Strings


/*
 * Crear elementos
 */

const div = document.createElement("div") // Crear un elemento div
body.append(div) // Añadir el div al body


/*
 * Modificar elementos
 */

// 1. Modificar texto
	// Ambos lo añaden de la misma forma pero en el output de
	// `console.log(div.textContent)` lo devuelve como se vería
	// en el HTML pero sin los tags (texto debajo y con indentación)
	// mientras que innerText es como se ve a nivel de css
	// (p.ej si es inline muestra Hola Adios) y sin indentación

	// Es mejor usar innerText
div.innerText = "Foo"
//div.textContent = "Foo"


// 2. Modificar HTML
	// 2.1 innerHTML (no recomendado por problemas de seguridad)
//div.innerHTML = "<strong>Hello Borld</strong>" // bold + world :)

	// 2.2 Crear y añadir un nuevo elemento de la forma normal
const strong = document.createElement("strong")
strong.innerText = "Hello Borld"
div.append(strong)

	// 2.3 Borrar y añadir un elemento
const div = document.querySelector("div")
const spanHi = document.querySelector("#hi")
const spanBye = document.querySelector("#bye")
spanBye.remove()
	// o div.removeChild(spanBye)
div.append(spanBye)


/*
 * Modificar atributos (id, class) de los elementos (<div>)
 */
spanHi.getAttribute("id")    // -> hi
	// o spanHi.id
spanHi.getAttribute("title") // -> hello
	// o spanHi.title
spanHi.setAttribute("id", "id_value") // -> <span id="id_value" title="hello">Hello</span>
	// o spanHi.id = "id_value"
spanHi.removeAttribute("id")

spanHi.dataset // <span data-test="texto"></span> ? DomStringMap { test: texto } // todos los datasets
spanHi.dataset.test // -> "texto" // acceder dataset en concreto
spanHi.dataset.newName = "new-name" // nuevo dataset

spanHi.classList.add("new-class")
spanHi.classList.remove("new-class")
spanHi.classList.toggle("hidden") // añadir si falta, borrar si existe
	// también se puede pasar true/false como .toggle("hidden", true)


/*
 * Modificar estilos del elemento (CSS)
 */
spanHi.style.color = "blue"
spanHi.style.backgroundColor = "yellow"
	// quitar guión y ponerlo camelCase
	// background-color -> backgroundColor
