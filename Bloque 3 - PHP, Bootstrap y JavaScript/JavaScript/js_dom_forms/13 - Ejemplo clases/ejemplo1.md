Object.__object__.

Object.setPrototypeOf()

Object.hasOwnProperty("propertyname") // devuelve -> true || false 



let animal = {
	eats: true
};

function Rabbit(name) {
	this.name = name;
}

Rabbit.prototype = animal;

let rabbit = new Rabbit("White Rabbit"); // rabbit.__proto__ == animal

alert(rabbit.eats); // -> true
