
//Handler para mostrar la información. 
function showContent(e) {
   console.log(e);
   alert("El elemento se originó en "+e.target.id+" y ahora estoy en"+this.id);
   
}

//Obtengo los divs para la demostración de Bubbling
let bubblingDivs = document.querySelectorAll("#bubbling div");
//Obtengo los divs para la demostración de Capturing
let capturingDivs = document.querySelectorAll("#capturing div");


//Añado los manejadores de eventos (click) para cada uno de ellos en Bubbling
// bubblingDivs.forEach(function(item) {
//     item.addEventListener('click',showContent);
// });

//Añado los manejadores de eventos (click) para cada uno de ellos en Capturing
//events of this type will be dispatched to the registered listener before 
//being dispatched to any EventTarget beneath it in the DOM tree
capturingDivs.forEach(function(item) {
    item.addEventListener('click',showContent, true);
});