/*
A partir del codigo que se entrega, programar estas funcionalidades:

    a   Añadir una tarea con la estructura HTML que se ve en el código:
        <div class="task">
            <p> [aqui va el nombre de la tarea] </p>
            <span class="close">X</span>
        </div>

    b   Añadir la funcionalidad de borrar tarea, de tal forma que haciendo click en
        la X, desaparezca de la lista ese elemento.
*/

const tasks = document.getElementsByClassName("tasks")[0]
const text = document.querySelector('input[type="text"]')
const buttonAdd = document.getElementsByClassName("btn")

const newTask = document.createElement("div")
const newP = document.createElement("p")
const newSpan = document.createElement("span")

buttonAdd.addEventListener("click", e => {
    newTask.classList.add("task")
    newP.append(text.value)
    newSpan.classList.add("close")
    newSpan.append("X")
    
    newTask.append(newP)
    newTask.append(newSpan)
    
    tasks.append(newTask)
})

