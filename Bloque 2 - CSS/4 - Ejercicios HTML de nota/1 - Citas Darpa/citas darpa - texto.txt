IBM desarrolla chip que se autodestruye 
Agencia de investigación científica militar DARPA ha encargado a IBM crear un chip CMOS que se convierte en polvo de silicio al recibir un comando específico por ondas de radio. 
"Es casi imposible detectar y recuperar artículos [electrónicos en el campo de batalla], lo que resulta en su acumulación no intencional en el medio ambiente, y el potencial uso no autorizado, y consiguiente riesgo para la propiedad intelectual y ventajas tecnológicas". 
Escribe DARPA en su sitio web, al referirse al programa VAPR. 
Fuente: www.diarioti.com 